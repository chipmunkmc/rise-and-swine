# rise-and-swine

Maintained fork of [Bad-Piggies-Original](https://github.com/BP-Innovation/Bad-Piggies-Original) (formerly known as `bad-piggies-2.3.6`)

Currently tested platforms: PC, Mac & Linux Standalone, Android, and WebGL.

# Why the name?

I suspect that the original name of Bad Piggies (from before the release) is "When Pigs Fly", which happens to be the name of an episode in the final game. Rise and Swine is also an episode of Bad Piggies, which took the place of When Pigs Fly as the second episode, which is why I named this project after it.

# Building

Firstly, clone the repository (`git clone https://code.chipmunk.land/ChipmunkMC/bad-piggies-2.3.6.git`), and then open it in the Unity Editor.

Next, switch to your preferred platform, and generate AssetBundles with Assets > Build AssetBundles.

Then, copy all files ending in `.unity3d` from the `Assets/AssetBundles` folder to `StreamingAssets/AssetBundles` (create the directory if it doesn't exist).

Now, you can either play in the editor, or build and run the game.
