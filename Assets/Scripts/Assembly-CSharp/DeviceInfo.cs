﻿using UnityEngine;

public class DeviceInfo
{
	public static bool UsesTouchInput
	{
		get
		{
			return Input.touchSupported && Input.touchCount != 0; // * Input.touchCount is checked so that if there are no touches, mouse input will be used (this is assummed to be called on every update)
		}
	}

	public static DeviceFamily ActiveDeviceFamily
	{
		get
		{
			return DeviceInfo.DeviceFamily.Android;
		}
	}

	public static bool IsDesktop
	{
		get
		{
			RuntimePlatform runtimePlatform = Application.platform;
			return runtimePlatform == RuntimePlatform.OSXEditor || runtimePlatform == RuntimePlatform.OSXPlayer || runtimePlatform == RuntimePlatform.WindowsPlayer || runtimePlatform == RuntimePlatform.WindowsEditor;
		}
	}

	public enum DeviceFamily
	{
		Ios,
		Android,
		Pc,
		Osx,
		BB10,
		WP8
	}
}
